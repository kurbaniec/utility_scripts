# PowerShell-scripts

## Sudo

1. Open PowerShell.
2. Copy the following script (Ctrl+C) and paste it in PowerShell (Alt+Space+E+P):

```
$script_path="$HOME\Documents\Scripts"; if (!(test-path $script_path)) {New-Item -ItemType directory $script_path} if (!(test-path $profile)) { new-item -path $profile -itemtype file -force }". $script_path\sudo.ps1" | Out-File $profile -append; "function sudo(){if (`$args.Length -eq 1){start-process `$args[0] -verb `"runAs`"} if (`$args.Length -gt 1){start-process `$args[0] -ArgumentList `$args[1..`$args.Length] -verb `"runAs`"}}" | Out-File $script_path\sudo.ps1; powershell
```

1. Hit Enter.

Maybe `set-executionpolicy remotesigned` is needed to enter in PowerShell to enable scripts.

### Source

* https://superuser.com/questions/42537/is-there-any-sudo-command-for-windows



## Touch

Open your PowerShell profile with `notepad $PROFILE`.

Add following line:

`function touch { if((Test-Path -Path ($args[0])) -eq $false) { set-content -Path ($args[0]) -Value ($null) } }`

### Source

* https://superuser.com/questions/502374/equivalent-of-linux-touch-to-create-an-empty-file-with-powershell
* https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_profiles?view=powershell-6