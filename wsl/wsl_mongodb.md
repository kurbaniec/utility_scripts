# MongoDB

MongoDB´s service `mongod` does not work natively on WSL. Therefore I made a alternative service called `mymongo`. 

To get it working, go to `wsl_scripts` and copy `mymongo` to you `/etc/init.d/` directory in your subsystem.

Then make it executable:

```bash
sudo chmod +x /etc/init.d/mymongo
```

Now you can start the service with:

```bash
sudo service mymongo start
```

Stop it with:

```bash
sudo service mymongo stop
```

How to start working:

```bash
sudo mongo
```

---

If you don´t want to use my service, you still can start mongo with:

```bash
sudo mongod --dbpath /var/data/db
```

## Sources

- [Installation](https://gist.github.com/Mikeysax/cc86c30903727c556bcce960f7e4d59b)
- [Kill Mongod](<https://stackoverflow.com/questions/11774887/how-to-stop-mongo-db-in-one-command>)

