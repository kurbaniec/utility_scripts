# PostgreSQL

Start the service:

```bash
sudo service postgresql start
```

How to start working:

```bash
sudo -i -u postgres
psql
```

or: 

```bash
sudo -u postgres psql
```

Exit postgres with `\q`

---

Add password for user postgres:

```bash
alter user postgres password 'PostgresPasswd1234';
```

## Sources

* [Installation](<https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04>)