# InfluxDB

InfluxDB has the same service problem as MongoDB. In the future I plan to add a custom service but for now use this:

Start the service:

```bash
sudo influxd
```

How to start working:

```bash
sudo influx -precision rfc3339
```

If you get an error like `open server: open tsdb store: cannot allocate memory` when starting the service, delete the databases at`/var/lib/influxdb/data`.

## Sources

- [Installation](<https://docs.influxdata.com/influxdb/v1.7/introduction/installation/>)

