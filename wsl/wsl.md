# Windows Subsystem for Linux - WSL

## Installation

*Disclaimer:* the following menu points are German, because I use Windows in this language. 

Open the search bar -> Einstellungen -> Update und Sicherheit -> Für Entwickler 

In this submenu you need to circle developer mode (Entwicklermodus). You will get a prompt, accept it with yes. The configuration now can take a while.

![wsl 1](img/wsl_1.png)



After the developer mode was set, open a new powershell window as an administrator and run following command to add the wsl feature to your machine:

```bash
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
```

Windows will install wsl, when finished you will need to restart your machine.

After reboot,you will have the feature installed, but you have to install a Linux distro of your choice for wsl, to use it.

I personally use Ubuntu (18), but you can choose another one. You will find the compatible distros in the Windows Store (I know it´s a bit weird).  Install one.

![wsl 2](img/wsl_2.png)

After installing you will find a new program called after you distribution. In my case, this was `Ubuntu 18.04` . It opens the bash for this subsystem. Alternatively you can search after `bash` or type `bash` in a powershell window. To exit it, just write `exit`.

That´s it.

## Programs and features

In the `wsl` directory in the repository you will find a `installer.sh`-script that will install some services. 

The installer includes for now following services:

* [PostgreSQL](wsl_postgresql.md)
* [MongoDB](wsl_mongodb.md)
* [Influxdb](wsl_influxdb.md)
* Vim
* Git

## Sources

* [WSL](<https://docs.microsoft.com/en-us/windows/wsl/install-win10>)
* [Create a service](<https://www.scalescale.com/tips/nginx/create-linux-daemon-service/>)
* [Reboot WSL only](<https://superuser.com/questions/1126721/rebooting-ubuntu-on-windows-without-rebooting-windows>)