#!/bin/bash

echo "creating mongodb service 'mymongo'"
#SCRIPT_PATH=`dirname "$0"`; SCRIPT_PATH=`eval "cd \"$SCRIPT_PATH\" && pwd"`
# test
#echo $SCRIPT_PATH
sudo apt install dos2unix

sudo touch /etc/init.d/mymongo 
#ls
sudo bash -c 'cat mymongo.sh > /etc/init.d/mymongo'
#sudo cp mymongo.sh /etc/init.d/mymongo
sudo chmod +x /etc/init.d/mymongo -v

echo "update"
sudo apt-get -y update

echo "upgrade"
sudo apt-get -y upgrade

echo "vim"
sudo apt-get -y install vim

echo "git"
sudo apt-get -y install git 

echo "postgesql"
sudo apt-get -y install postgresql

echo "mongodb"
cd ~
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
sudo apt-get update
sudo apt-get install -y mongodb-org
cd ~
cd /var/
sudo mkdir data
cd data
sudo mkdir db

#echo "creating mongodb service 'mymongo'"
#SCRIPT_PATH=`dirname "$0"`; SCRIPT_PATH=`eval "cd \"$SCRIPT_PATH\" && pwd"`
# test
#echo $SCRIPT_PATH
#sudo touch /etc/init.d/mymongo 
#ls
#sudo bash -c 'cat mymongo.sh > /etc/init.d/mymongo'
#sudo cp mymongo.sh /etc/init.d/mymongo
#sudo chmod +x /etc/init.d/mymongo -v


echo "influxdb"
wget https://dl.influxdata.com/influxdb/releases/influxdb_1.7.4_amd64.deb
sudo dpkg -i influxdb_1.7.4_amd64.deb

