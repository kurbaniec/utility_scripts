#!/bin/bash

NAME="mymongo"
DESC="MongoDB-deamon"

case "$1" in
start)
echo -n "Starting "
sudo mongod --dbpath /var/data/db > /dev/null &
echo "$DESC."
;;
stop)
echo -n "Stopping "
#sudo kill -2 `pgrep mongo`
mongo admin --eval "db.shutdownServer()" > /dev/null
echo "$DESC."
;;
restart)
echo -n "Restarting "
#sudo kill -2 `pgrep mongo`
mongo admin --eval "db.shutdownServer()" > /dev/null
sleep 1
sudo mongod --dbpath /var/data/db > /dev/null &
echo "$DESC."
;;
status)
ps -edaf | grep mongod | grep -v grep
;;
esac

exit 0