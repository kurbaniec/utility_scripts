# Utility_scripts

Scripts and tutorials for random stuff. *Really* random.

## Windows Subsystem for Linux - WSL

See [here](wsl/wsl.md).

## cmd_scripts
Unix-like commands for Powershell and CMD. Add folder to PATH.

## chocolatey
Install in Powershell with following command:
```bash
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

## Hyper CLI
```bash
choco install hyper
```

### Alt-Left-Arrow, Alt-Right-Arrow BUG

Change keymaps to following in the preferences:
```
keymaps: {
	"editor:moveNextWord": '',  
	"editor:movePreviousWord": "\x1b\x5b\x31\x3b\x35\x44"
	// or 'editor:movePreviousWord': '',
    // Example
    // 'window:devtools': 'cmd+alt+o',
  },
```


### hyper_plugins
Search!
```
"C:\Users\aon91\.hyper_plugins\node_modules\hyper-search\src\constants.js"
```
