const uuid = require('uuid');
const {MenuItem, Menu} = require("electron");

let shellConfig = [], shellMenus;
let showLaunchButton = false;

exports.decorateConfig = (config) => {
  shellConfig = config.otherShells || [];
  showLaunchButton =  config.otherShells === undefined ? false : true;
  return config;
};

function createShellMenus() {
  let menu = new Menu();

  shellConfig.forEach(shell => {
    menu.append(new MenuItem({
      label: shell.shellName,
      click(item, focusedWindow) {
        if (focusedWindow) {
          focusedWindow.rpc._events.new.call(undefined, {
            shell: shell.shell,
            shellArgs: shell.shellArgs
          });
        }
      }
    }));
  });

  return menu;
}

exports.decorateMenu = (menu) =>  {
    if (!showLaunchButton) return menu;
    shellMenus = createShellMenus();
    let item = new MenuItem({label: "Launch", submenu: shellMenus});
    menu.push(item);
    return menu;
}

exports.onWindow = (window) => {
  window.rpc.on('open launch menu', (coords) => {
    shellMenus.popup(coords);
  });

}

exports.decorateHeader = (Header, {React}) => {
  if (!showLaunchButton) {
    return Header;
  }

  return class extends React.Component {
    handleLaunchMenuClick(event) {
      console.dir(event);
      let {right: x, bottom: y} = event.currentTarget.getBoundingClientRect();
      x -= 15; // to compensate padding
      y -= 12; // ^ same
      rpc.emit('open launch menu', {x, y});
    }

    render() {
      let props = Object.assign({}, this.props);
      const css = {
        display: 'block',
        position: 'absolute',
        left: '4em',
        height: '100%'
      }
      const style = `
      .launch_menu {
        display: block;
        position: absolute;
        left: 4em;
        opacity: 0.5;
        width: 2em;
        z-index: 1000;
        -webkit-app-region: no-drag;
        cursor: default;
      }

      .launch_menu:hover {
        opacity: 1.0;
      }
      `
      const styleElem = React.createElement("style", {type: "text/css"}, style)
      props.customChildrenBefore =
        React.createElement(
          'div',
          {
            className: 'launch_menu header_shape.js',
            onClick: this.handleLaunchMenuClick
          },
          "++", props.customChildrenBefore, styleElem);
      return React.createElement(
        Header,
        Object.assign({}, props, {})
      );
    }
  }
}
